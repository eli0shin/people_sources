var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var pkg = require('./package.json');
var image = require('gulp-image');
var uglify = require('gulp-uglify');


var jsPath = 'public/js';

// Minify JS
gulp.task('minify-js', function () {
	return gulp.src([jsPath + '/jquery/jquery-2.1.4.js', jsPath + '/bootstrap/bootstrap-3.3.5.js', jsPath + '/affix.js', 'public/js/*.js'])
        .pipe(uglify())
        .pipe(concat('scripts.js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('public/js/min/'));
});

// Optimize images
gulp.task('images', function () {
	gulp.src('public/images/**/*')
        .pipe(image())
        .pipe(gulp.dest('public/images/'));
});


gulp.task('watch', function () {
	gulp.watch('public/js/*.js', ['minify-js']);
});

// Run everything
gulp.task('default', ['minify-js', 'watch']);
