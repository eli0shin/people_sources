/* $('.flexslider').flexslider({
	animation: "slide",
	controlsContainer: $(".custom-controls-container"),
    controlNav: false,
	pauseOnAction: false,
	animationLoop: true,
	itemWidth: 210,
    itemMargin: 15,
    minItems: 1,
    maxItems: 4
});
$(window).bind("resize", function(){
    $('.flexslider').flexslider(0);
});*/
// $(document).ready(function(){
// 	$('.your-class').slick({
// 		setting_name: setting_value
// 	});
// 	});
// $('.responsive').slick({
// 	dots: true,
// 	infinite: false,
// 	speed: 300,
// 	slidesToShow: 4,
// 	slidesToScroll: 4,
// 	responsive: [
// 		{
// 		breakpoint: 1024,
// 		settings: {
// 			slidesToShow: 3,
// 			slidesToScroll: 3,
// 			infinite: true,
// 			dots: true
// 		}
// 		},
// 		{
// 		breakpoint: 600,
// 		settings: {
// 			slidesToShow: 2,
// 			slidesToScroll: 2
// 		}
// 		},
// 		{
// 		breakpoint: 480,
// 		settings: {
// 			slidesToShow: 1,
// 			slidesToScroll: 1
// 		}
// 		}
// 		// You can unslick at a given breakpoint now by adding:
// 		// settings: "unslick"
// 		// instead of a settings object
// 	]
// 	});
$(".nav-link-scroll, .nav").find("a").click(function(e) {
	var section = $(this).attr("href");
	if (section[0] === '#'){
		e.preventDefault();
		$("html, body").animate({
			scrollTop: $(section).offset().top
		});
	}
});
$(function () {
	$('.navbar-collapse ul li a:not(.dropdown-toggle)').bind('click tap', function () {
		$('.navbar-toggle:visible').click();
	});
});
$(function () {
	var $window = $(window),
		$intro = $('.intro'),
		$windowHeight = $window.height(),
		$introHeight = $intro.height();
	if ($introHeight > $windowHeight) {
		$('.icon').hide();
		$('.social').css('margin-top', '5vh');
	}
});
$('#contact-form').on('submit', function () {
	$.ajax({
		url: '/',
		type: 'POST',
		data: $(this).serialize(),
		success: function (req, res) {
			// Show success message
			$('#send-message').show();
			$('#error-message').hide();
			// clear all fields
			$('#contact-form').trigger('reset');
		},
		error: function (req, res, err) {
			// Show fail message
			$('#error-message').show();
			$('#send-message').hide();
		},
	});
	return false;
});
// scroll to div ids
$(function() {
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 300);
				return false;
			}
		}
	});
});
