/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
var minifyHTML = require('express-minify-html');


// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
};

// Setup Route Bindings
exports = module.exports = function (app) {

	// minify html
	app.use(minifyHTML({
	    override: true,
	    exception_url: false,
	    htmlMinifier: {
	        removeComments: true,
	        collapseWhitespace: true,
	        collapseBooleanAttributes: true,
	        removeAttributeQuotes: true,
	        removeEmptyAttributes: true,
	        minifyJS: true,
	    },
	}));

	// Views
	app.all('/', routes.views.index);
	app.get('/team', routes.views.team);
	app.get('/team/:member', routes.views.teamMember);
	app.get('/blog/:category?', routes.views.blog);
	app.get('/blog/post/:post', routes.views.post);
	app.get('/events', routes.views.events);
	app.get('/events/:event', routes.views.event);
	app.get('/artists', routes.views.artists);
	app.get('/artists/:artist', routes.views.artist);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

};
