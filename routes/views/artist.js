var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'artists';
	locals.filters = {
		artist: req.params.artist,
	};
	locals.data = {
		artists: [],
	};

    // load artists
	view.on('init', function (next) {
		var q = keystone.list('artist').model.findOne({
			slug: locals.filters.artist,
		});

		q.exec(function (err, result) {
			locals.data.artist = result;
			next(err);
		});
	});

	// Render the view
	view.render('artist');
};
