var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	locals.formData = req.body || {};
	locals.validationErrors = {};
	locals.enquirySubmitted = false;

	// On POST requests, add the Enquiry item to the database
	view.on('post', { action: 'contact' }, function (next) {

		var newEnquiry = new Enquiry.model();
		var updater = newEnquiry.getUpdateHandler(req);

		updater.process(req.body, {
			flashErrors: true,
			fields: 'name, email, phone, subject, message',
			errorMessage: 'There was a problem submitting your enquiry:',
		}, function (err) {
			if (err) {
				locals.validationErrors = err.errors;
			} else {
				locals.enquirySubmitted = true;
			}
			next();
		});
	});

	// load intro tagline, logo, backgroundImage
	view.query('intro', keystone.list('intro').model.find().limit(1));

	// load social links
	view.query('social', keystone.list('socialLink').model.find());

	// load abouts
	view.query('about', keystone.list('about').model.find().limit(1));

	// load services
	view.query('services', keystone.list('service').model.find().where('state', 'published').sort('position'));

	// load team members
	view.query('team', keystone.list('teamMembers').model.find());

	// query featured events
	// view.query('featuredEvents', keystone.list('event').model.find()
	// 	.where('featuredEvent', 'true')
	// 	.sort('eventDate')
	// 	.limit(4));


	// Render the view
	view.render('index');
};
