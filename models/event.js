var keystone = require('keystone');
var Types = keystone.Field.Types;

var Event = new keystone.List('event', {
	autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' },
	defaultSort: 'eventDate',
});

Event.add({
	name: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
	featuredEvent: { type: Types.Boolean },
	eventDate: { type: Types.Date },
	eventUrl: { type: Types.Url },
	venue: { type: String },
	eventCity: { type: String },
	image: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
});


// Event.register();
