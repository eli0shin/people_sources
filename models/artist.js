var keystone = require('keystone');
var	Types = keystone.Field.Types;

var Artist = new keystone.List('artist', {
	autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' },
	defaultSort: '-createdAt',
});

Artist.add({
	name: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
	featuredArtist: { type: Types.Boolean },
	image: { type: Types.CloudinaryImage },
	artistFBPage: { type: Types.Url },
	artistWebPage: { type: Types.Url },
	bio: { type: Types.Html, wysiwyg: true, height: 400 },
});


// Artist.register();
