var keystone = require('keystone');
var Types = keystone.Field.Types;

var about = new keystone.List('about', {
	autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' },
	defaultSort: '-createdAt',
});

about.add({
	name: { type: String, required: true },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
});

about.register();
