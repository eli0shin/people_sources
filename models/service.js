var keystone = require('keystone');
var Types = keystone.Field.Types;

var Service = new keystone.List('service', {
	autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' },
	defaultSort: 'position',
});

Service.add({
	name: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
	position: { type: Types.Number },
	image: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
});


Service.register();
