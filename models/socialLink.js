var keystone = require('keystone');
var Types = keystone.Field.Types;

var socialLink = new keystone.List('socialLink', {
	autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' },
	defaultSort: '-createdAt',
});

socialLink.add({
	name: { type: String, required: true },
	link: { type: Types.Url },
	icon: { type: Types.Text },
});

socialLink.register();
