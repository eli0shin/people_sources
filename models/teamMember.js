var keystone = require('keystone');
var Types = keystone.Field.Types;

var teamMembers = new keystone.List('teamMembers', {
	autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' },
	defaultSort: '-createdAt',
});

teamMembers.add({
	name: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
	image: { type: Types.CloudinaryImage },
	duties: { type: Types.TextArray },
	fbUrl: { type: Types.Url },
	twitterHandle: { type: Types.Text },
	twitterLink: { type: Types.Url },
	email: { type: Types.Email, displayGravtar: true },
	bio: { type: Types.Html, wysiwyg: true, height: 400 },

});

teamMembers.register();
