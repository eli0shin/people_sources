var keystone = require('keystone');
var Types = keystone.Field.Types;

var Intro = new keystone.List('intro', {
	autokey: { path: 'slug', from: 'name', unique: true },
	map: { name: 'name' },
	defaultSort: '-createdAt',
	sortable: true,
});

Intro.add({
	name: { type: String, required: true },
	logoImage: { type: Types.CloudinaryImage },
	backgroundImage: { type: Types.CloudinaryImage },
	taglineHeader: { type: Types.Textarea },
	// taglineContent: { type: Types.Textarea },
});


Intro.register();
